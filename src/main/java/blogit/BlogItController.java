package blogit;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class BlogItController {
    @Autowired
    private BlogRepository blogRepository;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserSession userSession;
    
    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("title", "BlogIt: Home");
        model.addAttribute("user", userSession.getUser());
        model.addAttribute("blogs", blogRepository.findAll());
        return "index.html";
    }

    @GetMapping("/myblogs")
    public String myblogs(Model model) {
        model.addAttribute("title", "BlogIt: My Blogs");
        model.addAttribute("user", userSession.getUser());
        model.addAttribute("blogs", blogRepository.findAllByUsername(userSession.getUser().getUsername()));
        return "index.html";
    }

    @GetMapping("/blog")
    public String blog(@RequestParam("id") long id, Model model) {
        Blog blog = blogRepository.getOne(id);
        Post post = new Post();
        post.setBlog(blog);
        List<Post> posts = postRepository.findAll(Example.of(post));
        Collections.reverse(posts);
        model.addAttribute("title", "BlogIt: " + blog.getTitle());
        model.addAttribute("user", userSession.getUser());
        model.addAttribute("blog", blog);
        model.addAttribute("blogTitle", blog.getTitle());
        model.addAttribute("posts", posts);
        return "blog.html";
    }

    @GetMapping("/createPost")
    public String createPost(@RequestParam("id") long id, Model model) throws Exception {
        if (userSession.getUser() == null) {
            return "redirect:/login";
        } else {
            Blog blog = blogRepository.getOne(id);
            model.addAttribute("title", "BlogIt: " + blog.getTitle());
            model.addAttribute("user", userSession.getUser());
            model.addAttribute("blog", blog);
            model.addAttribute("blogTitle", blog.getTitle());
            return "createPost.html";
        }
    }

    @PostMapping("/createPost")
    public String createAPost(long blog_id, String title, String content) throws Exception {
        if (userSession.getUser() == null) {
            return "redirect:/login";
        } else {
            Blog blog = blogRepository.getOne(blog_id);
            Post post = new Post();
            post.setTitle(title);
            post.setContent(content);
            post.setBlog(blog);
            postRepository.save(post);
            return "redirect:/blog?id="+blog_id;
        }
    }

    @GetMapping("/post")
    public String post(@RequestParam("id") long id, Model model) {
        Optional<Post> optionalPost = postRepository.findById(id);
        Post post = optionalPost.get();

        model.addAttribute("title", "BlogIt: " + post.getBlog().getTitle());
        model.addAttribute("user", userSession.getUser());
        model.addAttribute("blogTitle", post.getBlog().getTitle());
        model.addAttribute("post", post);
        model.addAttribute("content", post.getContent().replaceAll("\n", "<BR>"));
        model.addAttribute("number_comments", post.getComments().size());
        Collections.reverse(post.getComments());
        model.addAttribute("comments", post.getComments());
        return "post.html";
    }

    @PostMapping("/comment")
    public String comment(String name, String message, long postId) throws IOException {
        Post post = postRepository.getOne(postId);
        Comment comment = new Comment();
        comment.setName(name);
        comment.setMessage(message);
        comment.setPost(post);
        commentRepository.save(comment);
        return "redirect:/post?id="+postId;
    }
}